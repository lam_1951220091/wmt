import { useRouter } from "next/router";
import { getClient, usePreviewSubscription } from "../../lib/sanity";
import { groq } from "next-sanity";
import Layout from "@components/Layout";
import NarrowContainer from "@components/Layout/NarrowContainer";
import Prose from "@components/Layout/Prose";
import Button from "@components/UI/Button";
import Filter from "@components/UI/Filter";
import { PreviewModeBanner } from "@components/UI/Banner";

const chapterIndexQuery = groq`{
  "siteConfig": *[_type == "siteconfig"][0],
  "pageData": *[_type == "page" && slug.current == "chapters"][0],
  "chapterData": *[_type == "chapter"] | order(publishedAt desc) {
    ..., 
    author[]->,
    tags[]->
  }
}`;

type ChapterPageProps = {
  chapterIndexData: {
    siteConfig: any;
    pageData: any;
    chapterData: any[];
  };
  preview?: boolean;
};

export default function Home(props: ChapterPageProps) {
  const { chapterIndexData, preview } = props;
  const { siteConfig, pageData, chapterData } = chapterIndexData;
  const router = useRouter();
  const { data: chapterPage } = usePreviewSubscription(chapterIndexQuery, {
    initialData: chapterIndexData,
    enabled: preview || router.query.preview !== undefined,
  });

  return (
    <Layout
      headerTitle={`${[pageData.title, pageData.titlePart2].join(" ")} | ${
        siteConfig.title
      }`}
      title={pageData.title}
      titlePart2={pageData.titlePart2}
      description={pageData.description}
      image={pageData.image}
      siteConfig={siteConfig}
    >
      {preview && <PreviewModeBanner />}
      <NarrowContainer>
        <div className="py-24">
          <Prose body={pageData.body} />
        </div>
        <div className="pb-24 flex justify-center space-x-12">
          <Button link="/call" text="Contribute a chapter" />
          <Button link="/resources" text="Other resources" />
          <Button link="/about" text="Learn more" />
        </div>
      </NarrowContainer>
      <Filter chapters={chapterData} />
      <NarrowContainer>
        <div className="py-24 flex justify-center space-x-12">
          <Button link="/call" text="Contribute a chapter" />
          <Button link="/resources" text="Other resources" />
          <Button link="/about" text="Learn more" />
        </div>
      </NarrowContainer>
    </Layout>
  );
}

export async function getStaticProps({ params, preview = false }) {
  const chapterPage = await getClient(preview).fetch(chapterIndexQuery);
  return {
    props: {
      chapterIndexData: chapterPage,
      preview,
    },
    revalidate: 10,
  };
}
