import { NextApiRequest, NextApiResponse } from "next";

async function sendDiscordMessage(requestBody: {
  name?: string;
  email?: string;
  comments?: string;
}) {
  const { name, email, comments } = requestBody;
  let result = "";
  if (name && email) {
    result += `${name} (${email})`;
  } else if (name) {
    result += name;
  } else if (email) {
    result += email;
  } else {
    result += "Someone";
  }

  if (email) {
    result += " added their email to the mailing list. ";
  } else {
    result += " submitted without adding their email to the mailing list. ";
  }

  if (comments) {
    result += `They had some comments: ${comments}`;
  } else {
    result += "They didn't have any comments.";
  }

  await fetch(process.env.DISCORD_WEBHOOK, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      content: result,
    }),
  });
}

async function addToMailchimp(requestBody: {
  name?: string;
  email?: string;
  comments?: string;
}) {
  const { name, email } = requestBody;
  await fetch(
    `https://us9.api.mailchimp.com/3.0/lists/${process.env.MAILCHIMP_LIST_ID}/members/${email}?skip_merge_validation=false`,
    {
      method: "PUT",
      headers: {
        Authorization: `Basic ${process.env.MAILCHIMP_KEY}`,
      },
      body: JSON.stringify({
        email_address: email,
        status: "subscribed",
        merge_fields: { NAME: name },
      }),
    }
  );
}

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  try {
    if (req.body.email) {
      await addToMailchimp(req.body);
      await sendDiscordMessage(req.body);
    } else {
      await sendDiscordMessage(req.body);
    }
    return res.status(200).json({ success: true });
  } catch (error) {
    console.log(error);
    return res.status(500).json({ error: "There was a problem" });
  }
}
