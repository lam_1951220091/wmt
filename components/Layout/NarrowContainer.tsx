type NarrowContainerProps = { children: any };
export default function NarrowContainer(props: NarrowContainerProps) {
  return (
    <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
      <div className="space-y-6 max-w-4xl mx-auto">{props.children}</div>
    </div>
  );
}
