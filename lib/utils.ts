export function getTagList(tags: Tag[], divider?: string) {
  const subjectTags = tags
    .filter((tag) => tag.type === "Subject")
    .map((tag) => tag.name)
    .sort();
  const geographyTags = tags
    .filter((tag) => tag.type === "Geography")
    .map((tag) => tag.name)
    .sort();
  return [...subjectTags, ...geographyTags].join(divider || " / ");
}

export const validEmail = (email: string) => {
  const re =
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
};
