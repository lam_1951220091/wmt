import Image from "next/image";
import Link from "next/link";
import { PortableText, PortableTextReactComponents } from "@portabletext/react";
import { imageBuilder } from "@lib/sanity";
import { BsArrow90DegUp, BsAsterisk } from "react-icons/bs";
import { H1, H2, H3, H4, A } from "./Text";

type ProseProps = {
  body: any;
};

const components: Partial<PortableTextReactComponents> = {
  block: {
    // Ex. 1: customizing common block types
    h1: ({ children }) => <H1>{children}</H1>,
    h2: ({ children }) => <H2>{children}</H2>,
    h3: ({ children }) => <H3>{children}</H3>,
    h4: ({ children }) => <H4>{children}</H4>,
    h5: ({ children }) => (
      <h5 className="font-semibold text-slate-900 ">{children}</h5>
    ),
    h6: ({ children }) => (
      <h6 className="font-semibold text-slate-900 italic">{children}</h6>
    ),
    blockquote: ({ children }) => (
      <blockquote className="pl-2 pr-2 pb-1 border-l-2 border-green-500 bg-slate-50">
        {children}
      </blockquote>
    ),
  },
  types: {
    span: ({ children }) => <span>{children}</span>,
    embeddedCode: ({ value }) => {
      return (
        <div className="space-y-1">
          <div
            className="flex place-content-center"
            dangerouslySetInnerHTML={{ __html: value.code }}
          />
          {value.caption && (
            <div className="flex place-content-center text-sm">
              <em>
                <PortableText value={value.caption} components={components} />
              </em>
            </div>
          )}
        </div>
      );
    },
    exampleContainer: ({ value }) => {
      return (
        <div id={value.id} className="example">
          <p className="text-slate-600">Please refresh to load examples</p>
        </div>
      );
    },
    image: ({ value }) => {
      return (
        <div className="space-y-1">
          <div className="flex place-content-center">
            <div className="w-96 h-96 relative">
              <a href={imageBuilder(value).url()} target="_blank">
                <Image
                  src={imageBuilder(value).url()}
                  alt={value.alt}
                  layout="fill"
                  objectFit="cover"
                />
              </a>
            </div>
          </div>
          {value.caption && (
            <div className="flex place-content-center text-sm">
              <em>
                <PortableText value={value.caption} components={components} />
              </em>
            </div>
          )}
        </div>
      );
    },
  },
  marks: {
    hangingIndent: ({ children }) => (
      <span className="block pl-6 -indent-6">{children}</span>
    ),
    footnote: ({ children, markKey }) => {
      return (
        <a id={`${markKey}-return`} href={`#${markKey}`}>
          {children}
          <sup>
            <BsAsterisk className="inline h-2 w-2" aria-hidden="true" />
            <span className="sr-only">See footnote</span>
          </sup>
        </a>
      );
    },
    strong: ({ children }) => (
      <strong className="font-semibold">{children}</strong>
    ),
    internalLink: ({ value, children }) => {
      const { _type, slug = {}, url } = value;
      let href = "";
      if (url) {
        href = url;
      } else {
        switch (value.type) {
          case "chapter":
            href = `/chapters/${slug.current}`;
            break;
          case "post":
            href = `/posts/${slug.current}`;
            break;
          default:
            href = `/${slug.current}`;
        }
      }
      return (
        <Link href={href} passHref>
          <A>{children}</A>
        </Link>
      );
    },
    externalLink: ({ value, children }) => {
      return (
        <Link href={value.href} target="_blank" prefetch={false} passHref>
          <A>{children}</A>
        </Link>
      );
    },
    link: ({ value, children }) => {
      return (
        <Link href={value.href} target="_blank" prefetch={false} passHref>
          <A>{children}</A>
        </Link>
      );
    },
  },
  list: {
    bullet: ({ children }) => (
      <ul className="ml-2 pl-5 -indent-4 mt-xl list-disc list-inside">
        {children}
      </ul>
    ),
    number: ({ children }) => (
      <ol className="ml-2 pl-5 -indent-5 mt-xl list-decimal list-inside">
        {children}
      </ol>
    ),
  },
};

export default function Prose(props: ProseProps) {
  const { body } = props;
  const notes = getFootnotes(body);
  return (
    <div className="text-slate-500 space-y-4">
      <PortableText value={body} components={components} />
      {notes.length > 0 && <Footnotes body={notes} />}
    </div>
  );
}

function Footnotes(props: ProseProps) {
  const { body } = props;
  return (
    <>
      <H2>Notes</H2>
      {body.map(({ _key, text }) => (
        <div className="flex" key={_key}>
          <div className="mr-4 flex-shrink-0 self-center">
            <a href={`#${_key}-return`}>
              <BsArrow90DegUp aria-hidden="true" />
              <span className="sr-only">Return to text</span>
            </a>
          </div>
          <div id={`${_key}`} key={_key}>
            <PortableText value={text} components={components} />
          </div>
        </div>
      ))}
    </>
  );
}

/**
 * Filter out anything that is not a block, reduce to get marks,
 * then filter out only footnote marks
 * @param body
 * @returns
 */
const getFootnotes = (body: any[]) => {
  return body
    .filter(({ _type }) => _type === "block")
    .reduce((acc, curr) => {
      return [...acc, ...curr.markDefs];
    }, [])
    .filter(({ _type }) => _type === "footnote");
};
