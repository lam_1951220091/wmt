import { VscSettingsGear } from "react-icons/vsc";

export default {
  name: "siteconfig",
  type: "document",
  title: "Site Settings",
  icon: VscSettingsGear,
  __experimental_actions: [
    /* "create", "delete", */
    "update",
    "publish",
  ],
  fields: [
    {
      name: "title",
      type: "string",
      title: "Site title",
      description:
        "Used in the site header/for SEO and for display on the home page",
      validation: (Rule) => Rule.required(),
    },
    {
      name: "copyrightYear",
      type: "string",
      title: "Copyright Year",
      description: "Used in the footer",
    },
    {
      name: "footerColumns",
      type: "array",
      description: "Footer columns",
      validation: (Rule) => Rule.required(),
      of: [
        {
          name: "column",
          title: "Column",
          description: "Column info",
          type: "object",
          validation: (Rule) => Rule.required(),
          fields: [
            {
              name: "header",
              title: "Heading",
              type: "string",
              validation: (Rule) => Rule.required(),
            },
            {
              name: "entries",
              title: "Entries",
              type: "array",
              validation: (Rule) => Rule.required(),
              of: [
                {
                  type: "object",
                  fields: [
                    {
                      name: "name",
                      title: "Name",
                      type: "string",
                      validation: (Rule) => Rule.required(),
                    },
                    {
                      name: "link",
                      title: "Link",
                      type: "url",
                      validation: (Rule) => Rule.required(),
                    },
                  ],
                },
              ],
            },
          ],
        },
      ],
    },
    {
      name: "social",
      type: "object",
      title: "Social and contact links",
      description: "Used in the footer",
      fields: [
        {
          name: "email",
          type: "string",
          title: "Email address",
        },
        {
          name: "facebook",
          type: "string",
          title: "Facebook URL",
        },
        {
          name: "instagram",
          type: "string",
          title: "Instagram URL",
        },
        {
          name: "twitter",
          type: "string",
          title: "Twitter URL",
        },
        {
          name: "gitlab",
          type: "string",
          title: "GitLab repo URL",
        },
      ],
    },
    {
      name: "homeImage",
      type: "image",
      title: "Homepage image",
      description: "Used in the site navigation bar",
      validation: (Rule) => Rule.required(),
      fields: [
        {
          name: "alt",
          type: "string",
          title: "Alternative text",
          description: "Important for SEO and accessiblity.",
          validation: (Rule) => Rule.required(),
        },
      ],
      options: {
        hotspot: true,
      },
    },
  ],
};
