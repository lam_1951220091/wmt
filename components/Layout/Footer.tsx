import { Formik, Form, Field, ErrorMessage } from "formik";
import {
  IoMailSharp,
  IoLogoFacebook,
  IoLogoInstagram,
  IoLogoTwitter,
  IoLogoGitlab,
} from "react-icons/io5";
import { validEmail } from "../../lib/utils";
import Script from "next/script";

type FooterProps = {
  siteConfig: {
    title: string;
    copyrightYear: string;
    footerColumns: {
      _key: string;
      header: string;
      entries: { _key: string; name: string; link: string }[];
    }[];
    social: {
      email?: string;
      facebook?: string;
      instagram?: string;
      twitter?: string;
      gitlab?: string;
    };
  };
};

export default function Footer(props: FooterProps) {
  return (
    <footer className="bg-slate-800" aria-labelledby="footer-heading">
      <h2 id="footer-heading" className="sr-only">
        Footer
      </h2>
      <div className="max-w-7xl mx-auto pt-12 space-y-12 px-4 sm:px-6 lg:pt-16 lg:px-8">
        <div className="md:grid md:grid-cols-2 md:gap-8 space-y-12 md:space-y-0">
          <div className="space-y-12">
            {props.siteConfig.footerColumns.map((column) => (
              <div key={column._key}>
                <h3 className="text-sm font-semibold text-gray-400 tracking-wider uppercase">
                  {column.header}
                </h3>
                <ul role="list" className="mt-4 space-y-4">
                  {column.entries.map((entry) => (
                    <li key={entry._key}>
                      <a
                        href={entry.link}
                        target="_blank"
                        className="text-base text-gray-300 hover:text-white"
                      >
                        {entry.name}
                      </a>
                    </li>
                  ))}
                </ul>
              </div>
            ))}
          </div>
          <div>
            <h3 className="text-sm font-semibold text-gray-400 tracking-wider uppercase">
              Leave feedback and subscribe
            </h3>
            <Formik
              initialValues={{ name: "", email: "", comments: "" }}
              validate={(values) => {
                const errors: { email?: string } = {};
                if (values.email !== "" && !validEmail(values.email)) {
                  errors.email = "Invalid email address";
                }
                return errors;
              }}
              onSubmit={async (values, { setSubmitting }) => {
                try {
                  const { name, email, comments } = values;
                  const res = await fetch("/api/signup", {
                    method: "POST",
                    headers: {
                      "Content-Type": "application/json",
                    },
                    body: JSON.stringify({ email, name, comments }),
                  });
                } catch (e) {
                  console.log(e);
                }
                setSubmitting(false);
              }}
            >
              {({ errors, isSubmitting }) => (
                <Form className="space-y-4">
                  <p className="mt-4 text-base text-gray-300">
                    Share your thoughts and add your email to sign up for our
                    newsletter. We don't share info and will not fill your
                    inbox.
                  </p>
                  <Field
                    component="textarea"
                    name="comments"
                    className="appearance-none min-w-0 w-full bg-white border border-transparent rounded-md py-2 px-4 text-base text-gray-900 placeholder-gray-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-800 focus:ring-white focus:border-white focus:placeholder-gray-400"
                    placeholder="Share your thoughts (optional)"
                  />
                  <Field
                    type="text"
                    name="name"
                    className="appearance-none min-w-0 w-full bg-white border border-transparent rounded-md py-2 px-4 text-base text-gray-900 placeholder-gray-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-800 focus:ring-white focus:border-white focus:placeholder-gray-400"
                    placeholder="Enter your name (optional)"
                  />
                  <Field
                    type="email"
                    name="email"
                    className="appearance-none min-w-0 w-full bg-white border border-transparent rounded-md py-2 px-4 text-base text-gray-900 placeholder-gray-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-800 focus:ring-white focus:border-white focus:placeholder-gray-400"
                    placeholder="Enter your email (optional)"
                  />
                  {errors.email && (
                    <p className="text-red-500">
                      <ErrorMessage name="email" />
                    </p>
                  )}
                  <button
                    type="submit"
                    disabled={isSubmitting}
                    className="mt-4 w-full bg-blue-500 border border-transparent rounded-md py-2 px-4 items-center justify-center text-base font-medium text-white disabled:bg-blue-100 disabled:text-blue-400 hover:bg-blue-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-800 focus:ring-blue-500"
                  >
                    Submit
                  </button>
                </Form>
              )}
            </Formik>
          </div>
        </div>
      </div>

      <div className="max-w-7xl mx-auto pb-12 px-4 sm:px-6 lg:pb-16 lg:px-8">
        <div className="mt-12 border-t border-gray-700 pt-8 lg:flex lg:items-center lg:justify-between">
          <div className="flex space-x-6 lg:order-2 lg:ml-20">
            {props.siteConfig.social.email && (
              <a
                href={`mailto:${props.siteConfig.social.email}`}
                className="text-gray-400 hover:text-gray-300"
              >
                <span className="sr-only">Email</span>
                <IoMailSharp className="h-6 w-6" aria-hidden="true" />
              </a>
            )}
            {props.siteConfig.social.facebook && (
              <a
                href={props.siteConfig.social.facebook}
                target="_blank"
                className="text-gray-400 hover:text-gray-300"
              >
                <span className="sr-only">Facebook</span>
                <IoLogoFacebook className="h-6 w-6" aria-hidden="true" />
              </a>
            )}
            {props.siteConfig.social.instagram && (
              <a
                href={props.siteConfig.social.instagram}
                target="_blank"
                className="text-gray-400 hover:text-gray-300"
              >
                <span className="sr-only">Instagram</span>
                <IoLogoInstagram className="h-6 w-6" aria-hidden="true" />
              </a>
            )}
            {props.siteConfig.social.twitter && (
              <a
                href={props.siteConfig.social.twitter}
                target="_blank"
                className="text-gray-400 hover:text-gray-300"
              >
                <span className="sr-only">Twitter</span>
                <IoLogoTwitter className="h-6 w-6" aria-hidden="true" />
              </a>
            )}
            {props.siteConfig.social.gitlab && (
              <a
                href={props.siteConfig.social.gitlab}
                target="_blank"
                className="text-gray-400 hover:text-gray-300"
              >
                <span className="sr-only">GitLab</span>
                <IoLogoGitlab className="h-6 w-6" aria-hidden="true" />
              </a>
            )}
          </div>
          <p className="space-y-4 mt-8 text-base text-gray-400 lg:mt-0 lg:order-1">
            <span className="block">
              &copy; {props.siteConfig.copyrightYear} {props.siteConfig.title}
            </span>
            <span className="block">
              Worldmusictextbook.org is open source and is maintained at{" "}
              <a
                href="https://gitlab.com/world-music-textbook"
                className="text-base text-gray-300 hover:text-white"
                target="_blank"
              >
                https://gitlab.com/world-music-textbook
              </a>
            </span>
            <span className="block">
              This work is licensed under a{" "}
              <a
                href="http://creativecommons.org/licenses/by-nc-nd/4.0/"
                className="text-base text-gray-300 hover:text-white"
                target="_blank"
              >
                Creative Commons Attribution — Non-Commercial — No Derivatives
                4.0 International License.
              </a>
            </span>
          </p>
        </div>
      </div>
      <Script
        data-goatcounter="https://worldmusic.goatcounter.com/count"
        strategy="afterInteractive"
        src="//gc.zgo.at/count.js"
      />
    </footer>
  );
}
