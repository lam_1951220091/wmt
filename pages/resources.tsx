import { useState } from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import { useRouter } from "next/router";
import { getClient, usePreviewSubscription } from "../lib/sanity";
import { groq } from "next-sanity";
import Layout from "@components/Layout";
import NarrowContainer from "@components/Layout/NarrowContainer";
import Prose from "@components/Layout/Prose";
import Filter from "@components/UI/Filter";
import Button from "@components/UI/Button";
import { PreviewModeBanner } from "@components/UI/Banner";
import { H1 } from "@components/Layout/Text";
import { validEmail } from "../lib/utils";

const resourcePageQuery = groq`{
  "siteConfig": *[_type == "siteconfig"][0],
  "page": *[_type == "page" && slug.current == "resources"]{
    ...,
    body[]{
      ...,
      markDefs[]{
        ...,
        _type == "internalLink" => {
          _key,
          "slug": @.reference->slug,
          "type": @.reference->_type,
        }
      }
    }
  }[0],
  "resources": *[_type == "resource"] | order(date desc) {
    ..., 
    tags[]->
  },
  "tags": *[_type == "tag"] | order(type desc, name asc)
}`;

function ResourceForm({ setShowForm }) {
  const [isSent, setIsSent] = useState<boolean>(false);

  return (
    <Formik
      initialValues={{ name: "", email: "", title: "", description: "" }}
      validate={(values) => {
        const errors: { email?: string; title?: string } = {};
        if (values.email !== "" && !validEmail(values.email)) {
          errors.email = "Invalid email address";
        }
        if (!values.title) errors.title = "Required";
        return errors;
      }}
      onSubmit={async (values, { setSubmitting, resetForm }) => {
        try {
          const { email, name, title, description } = values;
          const res = await fetch("/api/resource", {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify({ email, name, title, description }),
          });
          setIsSent(true);
          resetForm();
        } catch (e) {
          console.log(e);
        }
        setSubmitting(false);
      }}
    >
      {({ errors, isSubmitting, handleSubmit }) => (
        <Form className="space-y-4 mt-4">
          <p className="mt-4 text-base text-slate-500">
            Share your thoughts and add your email to sign up for our
            newsletter. We don't share info and will not fill your inbox.
          </p>
          {isSent && (
            <p className="mt-4 bg-blue-50 border border-blue-500 rounded-md py-2 px-4 text-base text-blue-500">
              Sent! Thanks for suggesting a new resource. Feel free to add more
              using the same form. If you provided contact info and we have
              questions, we will reach out. Otherwise, keep an eye here to see
              it once we can take a look.
            </p>
          )}
          <Field
            type="text"
            name="name"
            className="appearance-none min-w-0 w-full bg-white border rounded-md py-2 px-4 text-base text-gray-900 placeholder-gray-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-800 focus:ring-white focus:border-white focus:placeholder-gray-400"
            placeholder="Enter your name (optional)"
          />
          <Field
            type="email"
            name="email"
            className="appearance-none min-w-0 w-full bg-white border rounded-md py-2 px-4 text-base text-gray-900 placeholder-gray-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-800 focus:ring-white focus:border-white focus:placeholder-gray-400"
            placeholder="Enter your email (optional)"
          />
          {errors.email && (
            <p className="text-red-500">
              <ErrorMessage name="email" />
            </p>
          )}
          <Field
            type="text"
            name="title"
            className="appearance-none min-w-0 w-full bg-white border rounded-md py-2 px-4 text-base text-gray-900 placeholder-gray-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-800 focus:ring-white focus:border-white focus:placeholder-gray-400"
            placeholder="Resource title"
          />
          {errors.title && (
            <p className="text-red-500">
              <ErrorMessage name="title" />
            </p>
          )}
          <Field
            component="textarea"
            name="description"
            className="appearance-none min-w-0 w-full bg-white border rounded-md py-2 px-4 text-base text-gray-900 placeholder-gray-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-800 focus:ring-white focus:border-white focus:placeholder-gray-400"
            placeholder="A brief description of the resource (optional)"
          />
          <div className="rounded-md flex space-x-5">
            <button
              type="submit"
              disabled={isSubmitting}
              onClick={async (e) => {
                e.preventDefault();
                handleSubmit();
              }}
              className="w-full bg-blue-500 border border-transparent rounded-md py-2 px-4 items-center justify-center text-base font-medium text-white disabled:bg-blue-100 disabled:text-blue-400 hover:bg-blue-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500"
            >
              Submit
            </button>
            <button
              type="button"
              onClick={(e) => {
                e.preventDefault();
                setShowForm(false);
              }}
              className="w-full bg-red-500 border border-transparent rounded-md py-2 px-4 items-center justify-center text-base font-medium text-white disabled:bg-red-100 disabled:text-red-400 hover:bg-red-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-red-500"
            >
              Cancel
            </button>
          </div>
        </Form>
      )}
    </Formik>
  );
}

type ResourcesPageProps = {
  resourcePageData: {
    siteConfig: any;
    page: any;
    resources: Resource[];
    tags: Tag[];
  };
  preview?: boolean;
};

export default function ResourcesPage(props: ResourcesPageProps) {
  const [showForm, setShowForm] = useState<boolean>(false);
  const { resourcePageData, preview } = props;
  const router = useRouter();
  const { data: resourcePage } = usePreviewSubscription(resourcePageQuery, {
    initialData: resourcePageData,
    enabled: preview || router.query.preview !== undefined,
  });
  const { siteConfig, page, resources, tags } = resourcePage;
  return (
    <Layout
      headerTitle={`${[page.title, page.titlePart2].join(" ")} | ${
        siteConfig.title
      }`}
      title={page.title}
      titlePart2={page.titlePart2}
      description={page.description}
      image={page.image}
      cta={page.cta}
      siteConfig={siteConfig}
    >
      {preview && <PreviewModeBanner />}
      <NarrowContainer>
        <div className="pt-12">
          <Prose body={page.body} />
        </div>
        <Button
          text="Suggest a resource for our listing"
          onClick={() => setShowForm(!showForm)}
        />
        {showForm && <ResourceForm setShowForm={setShowForm} />}
      </NarrowContainer>

      <div className="py-12">
        <NarrowContainer>
          <div className="pb-12">
            <H1 center>Resource listing</H1>
          </div>
        </NarrowContainer>
        <Filter resources={resources} />
      </div>
    </Layout>
  );
}

export async function getStaticProps({ params, preview = false }) {
  const resourcePageData = await getClient(preview).fetch(resourcePageQuery);
  return {
    props: {
      resourcePageData,
      preview,
    },
    revalidate: 10,
  };
}
