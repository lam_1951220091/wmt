import React from "react";
import { ComponentStory, ComponentMeta } from "@storybook/react";

import TagList from "../components/UI/TagList";

export default {
  title: "UI/TagList",
  component: TagList,
  parameters: {
    // More on Story layout: https://storybook.js.org/docs/react/configure/story-layout
    layout: "fullscreen",
  },
} as ComponentMeta<typeof TagList>;

const Template: ComponentStory<typeof TagList> = (args) => (
  <TagList {...args} />
);

export const Default = Template.bind({});
Default.args = {
  tags: [
    {
      text: "Special",
    },
    {
      text: "Not special",
    },
    {
      text: "Something else",
    },
  ],
};

export const WithColor = Template.bind({});
WithColor.args = {
  tags: [
    {
      text: "Special",
      color: "green",
    },
    {
      text: "Not special",
      color: "blue",
    },
    {
      text: "Something else",
      color: "yellow",
    },
  ],
};
