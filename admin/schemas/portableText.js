import { HiLink, HiExternalLink } from "react-icons/hi";
import { IoFootstepsOutline } from "react-icons/io5";
import { BsTextRight } from "react-icons/bs";

export default {
  title: "Portable Text",
  name: "portableText",
  type: "array",
  of: [
    {
      type: "block",
      marks: {
        decorators: [
          { title: "Strong", value: "strong" },
          { title: "Emphasis", value: "em" },
          { title: "Underline", value: "underline" },
          {
            title: "Hanging indent",
            value: "hangingIndent",
            blockEditor: {
              icon: BsTextRight,
            },
          },
          { title: "Code", value: "code" },
          { title: "Strike", value: "strike-through" },
        ],
        annotations: [
          {
            title: "Internal link",
            name: "internalLink",
            type: "object",
            blockEditor: {
              icon: HiLink,
            },
            fields: [
              {
                name: "reference",
                type: "reference",
                to: [{ type: "page" }, { type: "post" }, { type: "chapter" }],
              },
            ],
          },
          {
            title: "External Link",
            name: "externalLink",
            type: "object",
            blockEditor: {
              icon: HiExternalLink,
            },
            fields: [
              {
                title: "URL",
                name: "href",
                type: "url",
                validation: (Rule) =>
                  Rule.uri({
                    allowRelative: true,
                    scheme: ["https", "http", "mailto", "tel"],
                  }),
              },
              {
                title: "Open in new tab",
                name: "blank",
                description: "Read https://css-tricks.com/use-target_blank/",
                type: "boolean",
                initialValue: true,
              },
            ],
          },
          {
            name: "footnote",
            type: "object",
            title: "Footnote",
            blockEditor: { icon: IoFootstepsOutline },
            fields: [
              {
                name: "text",
                type: "array",
                of: [
                  {
                    type: "block",
                    styles: [],
                    lists: [],
                    marks: {
                      decorators: [
                        { title: "Strong", value: "strong" },
                        { title: "Emphasis", value: "em" },
                        { title: "Code", value: "code" },
                      ],
                    },
                  },
                ],
              },
            ],
          },
        ],
      },
    },
    {
      type: "image",
      fields: [
        {
          name: "alt",
          type: "string",
          title: "Alternative text",
          description: "Important for accessiblity.",
          validation: (Rule) => Rule.required(),
        },
        {
          name: "caption",
          type: "array",
          title: "Caption",
          of: [{ type: "block" }],
        },
      ],
      options: {
        hotspot: true,
      },
    },
    {
      name: "embeddedCode",
      title: "Embedded code (Spotify/YouTube/etc)",
      type: "object",
      fields: [
        {
          name: "code",
          type: "text",
          title: "Embed code",
          description:
            "Use the compact/black options for Spotify unless there is a reason to do otherwise",
          validation: (Rule) => Rule.required(),
        },
        {
          name: "caption",
          type: "portableText",
          title: "Caption",
          description: "Shows like an image caption under the embedded object",
        },
      ],
    },
    {
      name: "exampleContainer",
      title: "Container for embedded code",
      type: "object",
      fields: [
        {
          name: "id",
          type: "string",
          title: 'Container ID ("example1")',
          description:
            "Use this to create an empty div element for use with embedded code",
          validation: (Rule) => Rule.required(),
        },
      ],
    },
  ],
};
