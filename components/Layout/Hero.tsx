import Link from "next/link";
import Image from "next/image";
import { useNextSanityImage } from "next-sanity-image";
import sanityClient from "@lib/sanity";
import { imageBuilder } from "../../lib/sanity";
import { Fragment } from "react";
import { Popover, Transition } from "@headlessui/react";
import { HiMenu, HiX, HiOutlineHome } from "react-icons/hi";

type HeroProps = {
  title?: string;
  titlePart2?: string;
  description?: string;
  author?: string;
  image?: any;
  cta?: {
    cta1?: { text: string; link: string };
    cta2?: { text: string; link: string };
  };
  homeImage?: any;
};

const navigation = [
  { name: "Chapters", href: "/chapters" },
  { name: "Resources", href: "/resources" },
  { name: "Submit", href: "/call" },
  { name: "About", href: "/about" },
  { name: "News", href: "/posts" },
];

export default function Hero(props: HeroProps) {
  const { title, titlePart2, description, author, image, cta, homeImage } =
    props;
  const imageProps = useNextSanityImage(sanityClient, image) || { src: "" };
  return (
    <div className="grid h-screen relative">
      <Image
        src={imageProps.src}
        className="-z-10 grayscale brightness-50"
        layout="fill"
        objectFit="cover"
        placeholder="blur"
        blurDataURL="/dark.png"
        alt="Hero image"
      />

      <Popover>
        <div className="pt-6 px-4 sm:px-6 lg:px-8">
          <nav
            className="relative flex items-center justify-between sm:h-10 lg:justify-start"
            aria-label="Global"
          >
            <div className="flex items-center flex-grow flex-shrink-0 lg:flex-grow-0">
              <div className="flex items-center justify-between w-full md:w-auto">
                <Link href="/">
                  <a>
                    <span className="sr-only">Home</span>
                    {homeImage ? (
                      <img
                        src={imageBuilder(homeImage).url()}
                        alt={homeImage.alt}
                        className="invert transition duration-300 hover:invert-0 hover:opacity-80 h-6 w-auto sm:h-8"
                      />
                    ) : (
                      <HiOutlineHome className="h-6 w-6" aria-hidden="true" />
                    )}
                  </a>
                </Link>
                <div className="-mr-2 flex items-center md:hidden">
                  <Popover.Button className="rounded-md p-2 inline-flex items-center justify-center text-slate-100 transition duration-300 hover:text-blue-900 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-blue-900">
                    <span className="sr-only">Open main menu</span>
                    <HiMenu className="h-6 w-6" aria-hidden="true" />
                  </Popover.Button>
                </div>
              </div>
            </div>
            <div className="hidden md:block md:ml-10 md:pr-4 md:space-x-8">
              {navigation.map((item) => (
                <Link href={item.href} key={item.name}>
                  <a className="font-medium text-slate-100 transition duration-300 hover:text-blue-900">
                    {item.name}
                  </a>
                </Link>
              ))}
            </div>
          </nav>
        </div>

        <Transition
          as={Fragment}
          enter="duration-150 ease-out"
          enterFrom="opacity-0 scale-95"
          enterTo="opacity-100 scale-100"
          leave="duration-100 ease-in"
          leaveFrom="opacity-100 scale-100"
          leaveTo="opacity-0 scale-95"
        >
          <Popover.Panel
            focus
            className="absolute z-10 top-0 inset-x-0 p-2 transition transform origin-top-right md:hidden"
          >
            <div className="rounded-lg shadow-md bg-white ring-1 ring-black ring-opacity-5 overflow-hidden">
              <div className="px-5 pt-4 flex items-center justify-between">
                <div>
                  <a href="/">
                    {homeImage ? (
                      <img
                        className="h-8 w-auto"
                        src={imageBuilder(homeImage).url()}
                        alt={homeImage.alt}
                      />
                    ) : (
                      <HiOutlineHome className="h-6 w-6" aria-hidden="true" />
                    )}
                  </a>
                </div>
                <div className="-mr-2">
                  <Popover.Button className="bg-white rounded-md p-2 inline-flex items-center justify-center text-slate-400 hover:text-slate-500 hover:bg-slate-100 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-blue-500">
                    <>
                      <span className="sr-only">Close main menu</span>
                      <HiX className="h-6 w-6" aria-hidden="true" />
                    </>
                  </Popover.Button>
                </div>
              </div>
              <div className="px-2 pt-2 pb-3 space-y-1">
                {navigation.map((item) => (
                  <Link key={item.name} href={item.href}>
                    <a className="block px-3 py-2 rounded-md text-base font-medium text-slate-700 hover:text-slate-900 hover:bg-slate-50">
                      {item.name}
                    </a>
                  </Link>
                ))}
              </div>
            </div>
          </Popover.Panel>
        </Transition>
      </Popover>

      <div className="z-0 self-end px-4 sm:px-8 pb-20 sm:pb-24">
        <div className="mb-0 text-left">
          <h1 className="text-5xl tracking-tight font-extrabold sm:text-6xl">
            <p className="block text-blue-200">{title}</p>
            <p className="block text-slate-200">{titlePart2}</p>
          </h1>
          <p className="mt-3 text-base text-slate-200 sm:mt-5 text-lg sm:max-w-xl sm:text-xl">
            {author || description}
          </p>
          {cta && (
            <div className="mt-5 sm:mt-8 sm:space-x-4 space-y-3 sm:space-y-0 sm:flex sm:justify-start">
              {cta.cta1 && (
                <Link href={cta.cta1.link}>
                  <a className="rounded-md transition duration-300 w-full sm:w-auto flex items-center justify-center px-8 py-3 sm:py-4 sm:text-lg text-base font-medium text-white bg-blue-600 hover:bg-blue-700">
                    {cta.cta1.text}
                  </a>
                </Link>
              )}
              {cta.cta2 && (
                <Link href={cta.cta2.link}>
                  <a className="rounded-md transition duration-300 w-full sm:w-auto flex items-center justify-center px-8 py-3 sm:py-4 sm:text-lg text-base font-medium text-blue-700 bg-blue-100 hover:bg-blue-200">
                    {cta.cta2.text}
                  </a>
                </Link>
              )}
            </div>
          )}
        </div>
      </div>
    </div>
  );
}
