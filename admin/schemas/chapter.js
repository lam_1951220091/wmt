import { HiOutlineBookOpen } from "react-icons/hi";

export default {
  name: "chapter",
  title: "Chapter",
  icon: HiOutlineBookOpen,
  type: "document",
  fields: [
    {
      name: "title",
      title: "Title",
      type: "string",
      validation: (Rule) => Rule.required(),
    },
    {
      name: "slug",
      title: "Slug",
      type: "slug",
      validation: (Rule) => Rule.required(),
      options: {
        source: "title",
        maxLength: 96,
      },
    },
    {
      name: "doiLink",
      title: "DOI/PDF link",
      description: "Link to the archive page or a downloadable PDF file",
      type: "url",
    },
    {
      name: "description",
      description: "Write a short pararaph/abstract of this chapter",
      title: "Description",
      rows: 5,
      type: "text",
      validation: (Rule) => [
        Rule.max(160).warning("SEO descriptions are best under 160 characters"),
        Rule.max(350).error("Maximum of 350 characters"),
        Rule.required(),
      ],
    },
    {
      name: "multiPage",
      title: "Multi-page chapter",
      type: "boolean",
      initialValue: false,
    },
    {
      name: "body",
      title: "Body",
      type: "portableText",
      hidden: ({ parent }) => parent.multiPage,
    },
    {
      name: "multiPageBody",
      title: "Sections",
      type: "array",
      of: [
        {
          type: "object",
          fields: [
            { name: "sectionTitle", title: "Section title", type: "string" },
            {
              name: "sectionBody",
              title: "Section body",
              type: "portableText",
            },
          ],
        },
      ],
      hidden: ({ parent }) => !parent.multiPage,
    },
    {
      name: "author",
      title: "Author",
      type: "array",
      of: [{ type: "reference", to: [{ type: "author" }] }],
      validation: (Rule) => Rule.required(),
    },
    {
      name: "image",
      title: "Hero image",
      type: "image",
      fields: [
        {
          name: "alt",
          type: "string",
          title: "Alternative text",
          description: "Important for SEO and accessiblity.",
          options: {
            isHighlighted: true,
          },
        },
      ],
      options: {
        hotspot: true,
      },
      validation: (Rule) => Rule.required(),
    },
    {
      name: "tags",
      title: "Tags",
      type: "array",
      of: [{ type: "reference", to: { type: "tag" } }],
      validation: (Rule) => Rule.required(),
    },
    {
      name: "codePath",
      title: "Code path",
      description:
        'Location of "main" import within the lib/code folder (example: test/index)',
      type: "string",
    },
    {
      name: "publishedAt",
      title: "Published at",
      type: "datetime",
    },
  ],

  preview: {
    select: {
      title: "title",
      author1: "author.0.name",
      author2: "author.1.name",
      author3: "author.2.name",
      media: "image",
    },
    prepare(selection) {
      const { title, author1, author2, author3 } = selection;
      const authors = [author1, author2].filter(Boolean);
      const subtitle = authors.join(", ");
      return {
        title,
        subtitle: author3 ? `${subtitle}...` : subtitle,
      };
    },
  },
};
