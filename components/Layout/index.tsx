import Head from "next/head";
import Hero from "./Hero";
import Footer from "./Footer";

type HeadComponentTypes = {
  title: string;
  description?: string;
  author?: string;
  keywords?: string | string[];
};

function HeadComponent(props: HeadComponentTypes) {
  const { title, description, author, keywords } = props;
  return (
    <Head>
      <title>{title}</title>
      <meta property="og:title" content={title} key="title" />
      {description && (
        <meta name="description" content={description} key="description" />
      )}
      {author && <meta name="author" content={author} key="author" />}
      {keywords && (
        <meta
          name="keywords"
          content={Array.isArray(keywords) ? keywords?.join(", ") : keywords}
          key="keywords"
        />
      )}
      <meta name="viewport" content="initial-scale=1.0, width=device-width" />
    </Head>
  );
}

type LayoutProps = {
  headerTitle: string;
  title: string;
  titlePart2?: string;
  description: string;
  image?: Image;
  author?: string;
  keywords?: string | string[];
  siteConfig: any;
  children: any;
  preview?: boolean;
  cta?: any;
};

export default function Layout(props: LayoutProps) {
  return (
    <>
      <HeadComponent
        title={props.headerTitle}
        description={props.description}
        author={props.author}
        keywords={props.keywords}
      />
      <Hero
        title={props.title}
        titlePart2={props.titlePart2}
        description={props.description}
        author={props.author}
        image={props.image}
        cta={props.cta}
        homeImage={props.siteConfig.homeImage}
      />
      {props.children}
      <Footer siteConfig={props.siteConfig} />
    </>
  );
}
