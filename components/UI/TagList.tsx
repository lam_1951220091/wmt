import { HiOutlineTag } from "react-icons/hi";
import Tag from "./Tag";

type TagListProps = {
  tags: { text: string; color?: string }[];
};

export default function TagList(props) {
  const { tags } = props;
  return (
    <div className="space-x-1">
      <HiOutlineTag className="inline align-middle text-gray-500 h-4 w-4" />
      {tags.map((tag: { text: string; color?: string }) => (
        <Tag text={tag.text} color={tag.color} key={tag.text} />
      ))}
    </div>
  );
}
