import React from "react";
import { ComponentStory, ComponentMeta } from "@storybook/react";

import ListContainer from "../components/Layout/ListContainer";

export default {
  title: "Layout/ListContainer",
  component: ListContainer,
  parameters: {
    // More on Story layout: https://storybook.js.org/docs/react/configure/story-layout
    layout: "fullscreen",
  },
} as ComponentMeta<typeof ListContainer>;

const Template: ComponentStory<typeof ListContainer> = (args) => (
  <ListContainer {...args} />
);

export const Default = Template.bind({});
Default.args = {
  items: [
    { key: "Thing one", content: "Thing one" },
    { key: "Thing two", content: "Thing two" },
    { key: "Thing three", content: "Thing three" },
  ],
};
