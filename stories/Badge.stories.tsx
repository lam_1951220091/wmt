import React from "react";
import { ComponentStory, ComponentMeta } from "@storybook/react";

import Badge from "../components/UI/Badge";

export default {
  title: "UI/Badge",
  component: Badge,
  parameters: {
    // More on Story layout: https://storybook.js.org/docs/react/configure/story-layout
    layout: "fullscreen",
  },
} as ComponentMeta<typeof Badge>;

const Template: ComponentStory<typeof Badge> = (args) => <Badge {...args} />;

export const Default = Template.bind({});
Default.args = {
  text: "Special",
};

export const WithColor = Template.bind({});
WithColor.args = {
  text: "Special",
  color: "red",
};
