import { HiOutlineNewspaper } from "react-icons/hi";

export default {
  name: "post",
  title: "Post",
  icon: HiOutlineNewspaper,
  type: "document",
  fields: [
    {
      name: "title",
      title: "Title",
      type: "string",
      validation: (Rule) => Rule.required(),
    },
    {
      name: "slug",
      title: "Slug",
      type: "slug",
      validation: (Rule) => Rule.required(),
      options: {
        source: "title",
        maxLength: 96,
      },
    },
    {
      name: "description",
      description: "Write a short pararaph of this post (For SEO Purposes)",
      title: "Description",
      rows: 5,
      type: "text",
      validation: (Rule) =>
        Rule.max(160).error(
          "SEO descriptions are usually better when its below 160"
        ),
    },
    {
      name: "body",
      title: "Body",
      type: "portableText",
      validation: (Rule) => Rule.required(),
    },
    {
      name: "author",
      title: "Author",
      type: "array",
      of: [{ type: "reference", to: [{ type: "author" }] }],
      validation: (Rule) => Rule.required(),
    },
    {
      name: "image",
      title: "Hero image",
      type: "image",
      fields: [
        {
          name: "alt",
          type: "string",
          title: "Alternative text",
          description: "Important for SEO and accessiblity.",
          options: {
            isHighlighted: true,
          },
        },
      ],
      options: {
        hotspot: true,
      },
      validation: (Rule) => Rule.required(),
    },
    {
      name: "publishedAt",
      title: "Published at",
      type: "datetime",
    },
  ],

  preview: {
    select: {
      title: "title",
      author1: "author.0.name",
      author2: "author.1.name",
      author3: "author.2.name",
      media: "image",
    },
    prepare(selection) {
      const { title, author1, author2, author3 } = selection;
      const authors = [author1, author2].filter(Boolean);
      const subtitle = authors.join(", ");
      return {
        title,
        subtitle: author3 ? `${subtitle}...` : subtitle,
      };
    },
  },
};
