import { part1setup } from "./parts/part1.js";
import { part2setup } from "./parts/part2.js";
import { part3setup } from "./parts/part3.js";
import { part4setup } from "./parts/part4.js";
import { examples } from "./examples.js";

export function main() {
  part1setup(examples);
  part2setup(examples);
  part3setup(examples);
  part4setup(examples);
}
