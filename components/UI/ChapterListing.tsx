import Link from "next/link";
import Image from "next/image";
import { useNextSanityImage } from "next-sanity-image";
import sanityClient from "@lib/sanity";
import { HiOutlineTag } from "react-icons/hi";
import { getTagList } from "../../lib/utils";

type ChapterListingProps = {
  chapter: Chapter;
  index: number;
};

export default function ChapterListing(props: ChapterListingProps) {
  const { chapter, index } = props;
  const publishedAt = new Date(chapter.publishedAt);
  const leftBody = (
    <div className="flex-1 flex items-center justify-between">
      <div className="h-full w-full pr-4 pl-10 pb-8 pt-6 sm:pt-12 sm:pb-16 text-slate-500 transition duration-300 group-hover:bg-slate-100 group-hover:text-slate-900">
        <div className="text-right">
          <p className="inline-block border-b-4 border-slate-500 font-light">
            {publishedAt.toLocaleDateString([], { dateStyle: "long" })}
          </p>
        </div>
        <h2 className="mt-2 text-2xl font-bold text-right">{chapter.title}</h2>
        {chapter.author && (
          <p className="font-semibold text-right">
            By {chapter.author.map((a) => a.name).join(", ")}
          </p>
        )}
        {chapter.description && (
          <p className="mt-2 text-sm font-light text-right">
            {chapter.description}
          </p>
        )}
        {chapter.tags && (
          <p className="mt-2 text-sm font-light text-right">
            <HiOutlineTag
              className="inline align-middle text-gray-500 h-4 w-4 mr-1"
              aria-hidden="true"
            />
            {getTagList(chapter.tags)}
          </p>
        )}
      </div>
    </div>
  );

  const rightBody = (
    <div className="flex-1 flex items-center justify-between">
      <div className="h-full w-full pl-4 pr-10 pb-8 pt-6 sm:pt-12 sm:pb-16 text-slate-500 transition duration-300 group-hover:bg-slate-100 group-hover:text-slate-900">
        <p className="inline-block border-b-4 border-slate-500 font-light">
          {publishedAt.toLocaleDateString([], { dateStyle: "long" })}
        </p>
        <h2 className="mt-2 text-2xl font-bold">{chapter.title}</h2>
        {chapter.author && (
          <p className="font-semibold">
            By {chapter.author.map((a) => a.name).join(", ")}
          </p>
        )}
        {chapter.description && (
          <p className="mt-2 text-sm font-light">{chapter.description}</p>
        )}
        {chapter.tags && (
          <p className="mt-2 text-sm font-light">
            <HiOutlineTag
              className="inline align-middle text-gray-500 h-4 w-4 mr-1"
              aria-hidden="true"
            />
            {getTagList(chapter.tags)}
          </p>
        )}
      </div>
    </div>
  );

  const imageProps = useNextSanityImage(sanityClient, chapter.image) || {
    src: "",
    blurDataURL: "",
  };
  const image = (
    <div
      className={
        "transition duration-300 group-hover:opacity-70 group-hover:grayscale relative flex-shrink-0 flex items-center justify-center w-28 sm:w-52 min-h-28"
      }
    >
      <Image
        src={imageProps.src}
        blurDataURL={imageProps.blurDataURL}
        layout="fill"
        objectFit="cover"
        placeholder="blur"
        alt="Chapter hero image"
      />
    </div>
  );

  return (
    <Link href={`/chapters/${chapter.slug.current}`}>
      <a className="flex cursor-pointer group">
        {index % 2 === 0 ? (
          <>
            {leftBody}
            {image}
          </>
        ) : (
          <>
            {image}
            {rightBody}
          </>
        )}
      </a>
    </Link>
  );
}
