// Any random string, must match SANITY_PREVIEW_SECRET in the Next.js .env.local file
const previewSecret = "cRRmQheMUHstGWf3r2ppceA2y4tPaT6s";
const remoteUrl = `https://wmt-coral.vercel.app`;
const localUrl = `http://localhost:3000`;

export default function resolveProductionUrl(doc) {
  const baseUrl =
    window.location.hostname === "localhost" ? localUrl : remoteUrl;

  const previewUrl = new URL(baseUrl);

  previewUrl.pathname = `/api/preview`;
  previewUrl.searchParams.append(`secret`, previewSecret);
  if (doc?.slug?.current) {
    previewUrl.searchParams.append(`slug`, doc.slug.current);
  }

  return previewUrl.toString();
}
