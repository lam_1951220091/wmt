import { useRouter } from "next/router";
import { getClient, usePreviewSubscription } from "../../lib/sanity";
import { groq } from "next-sanity";
import Layout from "@components/Layout";
import NarrowContainer from "@components/Layout/NarrowContainer";
import ListContainer from "@components/Layout/ListContainer";
import PostListing from "@components/UI/PostListing";
import Prose from "@components/Layout/Prose";
import { PreviewModeBanner } from "@components/UI/Banner";

const postIndexQuery = groq`{
  "siteConfig": *[_type == "siteconfig"][0],
  "page": *[_type == "page" && slug.current == "posts"][0],
  "posts": *[_type == "post"] | order(publishedAt desc) {
    ..., 
    author[]->,
    tags[]->
  }
}`;

type HomeProps = {
  postIndexData: any;
  preview?: boolean;
};

export default function Home(props: HomeProps) {
  const { postIndexData, preview } = props;
  const router = useRouter();
  const { data: postIndex } = usePreviewSubscription(postIndexQuery, {
    initialData: postIndexData,
    enabled: preview || router.query.preview !== undefined,
  });
  const { siteConfig, page, posts } = postIndex;
  return (
    <Layout
      headerTitle={`${[page.title, page.titlePart2].join(" ")} | ${
        siteConfig.title
      }`}
      title={page.title}
      titlePart2={page.titlePart2}
      description={page.description}
      image={page.image}
      siteConfig={siteConfig}
    >
      {preview && <PreviewModeBanner />}
      <NarrowContainer>
        <div className="py-24">
          <Prose body={page.body} />
          {posts && (
            <ListContainer>
              {posts.map((post) => ({
                key: post._id,
                content: <PostListing post={post} />,
              }))}
            </ListContainer>
          )}
        </div>
      </NarrowContainer>
    </Layout>
  );
}

export async function getStaticProps({ params, preview = false }) {
  const postIndexData = await getClient(preview).fetch(postIndexQuery);
  return {
    props: {
      postIndexData,
      preview,
    },
    revalidate: 10,
  };
}
