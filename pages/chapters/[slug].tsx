import { useRouter } from "next/router";
import { useState, useEffect } from "react";
import { getClient, usePreviewSubscription } from "@lib/sanity";
import { HiOutlineTag, HiOutlineDownload } from "react-icons/hi";
import { groq } from "next-sanity";
import Layout from "@components/Layout";
import NarrowContainer from "@components/Layout/NarrowContainer";
import Prose from "@components/Layout/Prose";
import Author from "@components/UI/Author";
import { PreviewModeBanner } from "@components/UI/Banner";
import { H1, H2, A } from "@components/Layout/Text";
import { getTagList } from "../../lib/utils";

const siteConfigQuery = groq`*[_type == "siteconfig"][0]`;

function MultiPageBody({ sections, codePath }) {
  const [page, setPage] = useState(0);
  useEffect(() => {
    if (codePath) {
      import(`../../lib/code/${codePath}`).then((module) => {
        module.main();
      });
    }
  });

  const titles = sections.map((s) => s.sectionTitle);
  const buttons = (
    <div className="text-center sm:text-left bg-green-50 rounded-md pt-1 pb-3 px-3">
      <p className="font-light">This chapter has multiple parts</p>
      <span className="text-2xl font-light mr-2">Go to</span>
      {titles.map(
        (title, i) =>
          i !== page && (
            <span
              key={title}
              onClick={() => {
                const anchor = document.getElementById("scrollAnchor");
                setPage(i);
                anchor.scrollIntoView();
              }}
              className="cursor-pointer mr-2 text-2xl font-light transition duration-300 text-slate-500 hover:text-slate-900 border-b-4 border-slate-500 hover:border-slate-900"
            >
              {title}
            </span>
          )
      )}
    </div>
  );

  return (
    <div id="scrollAnchor" className="space-y-4">
      <h2 className="text-4xl font-bold text-slate-900">
        {sections[page].sectionTitle}
      </h2>
      {buttons}
      <Prose body={sections[page].sectionBody} />
      {buttons}
    </div>
  );
}

type ChapterProps = {
  slug: Slug;
  siteConfigData: any;
  chapterData: Chapter;
  preview?: boolean;
};

export default function Chapter(props: ChapterProps) {
  const { slug, siteConfigData, chapterData, preview } = props;
  const chapterQuery = groq`*[_type == "chapter" && slug.current == "${slug}"]{
    ..., 
    body[]{
      ...,
      markDefs[]{
        ...,
        _type == "internalLink" => {
          _key,
          "slug": @.reference->slug,
          "type": @.reference->_type,
        }
      }
    },
    author[]->,
    tags[]->,
  }[0]`;
  const router = useRouter();
  const { data: siteConfig } = usePreviewSubscription(siteConfigQuery, {
    initialData: siteConfigData,
    enabled: preview || router.query.preview !== undefined,
  });
  const { data: chapter } = usePreviewSubscription(chapterQuery, {
    initialData: chapterData,
    enabled: preview || router.query.preview !== undefined,
  });

  useEffect(() => {
    if (!chapterData.multiPage && chapterData.codePath) {
      import(`../../lib/code/${chapterData.codePath}`).then((module) => {
        module.main();
      });
    }
  });

  return (
    <Layout
      headerTitle={`${chapter.title} | ${siteConfig.title}`}
      title={chapter.title}
      description={chapter.description}
      author={chapter.author.map((author) => author.name).join(" / ")}
      image={chapter.image}
      siteConfig={siteConfig}
    >
      {preview && <PreviewModeBanner />}
      <div id="chapterBody" className="py-24">
        <NarrowContainer>
          <H1>{chapter.title}</H1>
          <H2>by {chapter.author.map((author) => author.name).join(", ")}</H2>
          <div className="space-y-2">
            {chapter.description && (
              <p className="text-sm font-light">{chapter.description}</p>
            )}
            {chapter.tags || chapter.doiLink ? (
              <p className="mt-2 text-sm font-light">
                {chapter.tags && (
                  <span className="mr-2">
                    <HiOutlineTag
                      className="inline align-middle text-gray-500 h-4 w-4 mr-1"
                      aria-hidden="true"
                    />
                    {getTagList(chapter.tags)}
                  </span>
                )}
                {chapter.doiLink && (
                  <A href={chapter.doiLink}>
                    <HiOutlineDownload
                      className="inline align-middle text-gray-500 h-4 w-4 mr-1"
                      aria-hidden="true"
                    />
                    Download
                  </A>
                )}
              </p>
            ) : null}
          </div>
          {chapter.multiPage ? (
            <MultiPageBody
              sections={chapter.multiPageBody}
              codePath={chapter.codePath}
            />
          ) : (
            <Prose body={chapter.body} />
          )}
          {chapter.author.map((author) => (
            <Author author={author} key={author._id} />
          ))}
        </NarrowContainer>
      </div>
    </Layout>
  );
}

export async function getStaticPaths() {
  const slugs = await getClient().fetch(groq`*[_type == 'chapter']{slug}`);
  return {
    paths: slugs.map((obj: { slug: { current: string } }) => ({
      params: {
        slug: obj.slug.current,
      },
    })),
    fallback: false,
  };
}

export async function getStaticProps({ params, preview = false }) {
  const chapterQuery = groq`*[_type == "chapter" && slug.current == "${params.slug}"]{
    ...,
    body[]{
      ...,
      markDefs[]{
        ...,
        _type == "internalLink" => {
          _key,
          "slug": @.reference->slug,
          "type": @.reference->_type,
        }
      }
    },
    author[]->,
    tags[]->,
  }[0]`;
  const siteConfig = await getClient(preview).fetch(siteConfigQuery);
  const chapter = await getClient(preview).fetch(chapterQuery);
  return {
    props: {
      slug: params.slug,
      siteConfigData: siteConfig,
      chapterData: chapter,
      preview,
    },
    revalidate: 10,
  };
}
