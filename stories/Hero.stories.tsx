import React from "react";
import { ComponentStory, ComponentMeta } from "@storybook/react";

import Hero from "../components/Layout/Hero";

export default {
  title: "Layout/Hero",
  component: Hero,
  parameters: {
    // More on Story layout: https://storybook.js.org/docs/react/configure/story-layout
    layout: "fullscreen",
  },
} as ComponentMeta<typeof Hero>;

const Template: ComponentStory<typeof Hero> = (args) => <Hero {...args} />;

export const Default = Template.bind({});
Default.args = {
  title: "Title",
  titlePart2: "Part 2",
  description:
    "And a good description of what's on the page. And a good description of what's on the page. And a good description of what's on the page. And a good description of what's on the page.",
  cta: {
    cta1: { text: "Button 1", link: "https://example.com" },
    cta2: { text: "Button 2", link: "https://example.com" },
  },
};

export const OneButton = Template.bind({});
OneButton.args = {
  title: "Title",
  titlePart2: "Part 2",
  description:
    "And a good description of what's on the page. And a good description of what's on the page. And a good description of what's on the page. And a good description of what's on the page.",
  cta: {
    cta1: { text: "Button 1", link: "https://example.com" },
  },
};

export const NoButtons = Template.bind({});
NoButtons.args = {
  title: "Title",
  titlePart2: "Part 2",
  description:
    "And a good description of what's on the page. And a good description of what's on the page. And a good description of what's on the page. And a good description of what's on the page.",
};
