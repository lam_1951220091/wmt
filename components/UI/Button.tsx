import Link from "next/link";
import { MouseEvent } from "react";

type ButtonProps = {
  text: string;
  onClick?: (event: MouseEvent) => void;
  link?: string;
};

export default function Button(props: ButtonProps) {
  const { text, link, onClick } = props;
  if (onClick)
    return (
      <button
        onClick={onClick}
        className="cursor-pointer text-center text-2xl font-light transition duration-300 text-slate-500 hover:text-slate-900 border-b-4 border-slate-500 hover:border-slate-900"
      >
        {text}
      </button>
    );
  return (
    <Link href={link}>
      <a className="cursor-pointer text-center text-2xl font-light transition duration-300 text-slate-500 hover:text-slate-900 border-b-4 border-slate-500 hover:border-slate-900">
        {text}
      </a>
    </Link>
  );
}
