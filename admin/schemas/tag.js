import { HiOutlineTag } from "react-icons/hi";

export default {
  name: "tag",
  title: "Tag",
  type: "document",
  icon: HiOutlineTag,
  fields: [
    {
      name: "name",
      title: "Name",
      type: "string",
      validation: (Rule) => Rule.isRequired(),
    },
    {
      name: "type",
      title: "Tag type",
      type: "string",
      validation: (Rule) => Rule.isRequired(),
      options: {
        list: ["Subject", "Geography"],
        layout: "radio",
      },
    },
  ],
  preview: {
    select: {
      title: "name",
      subtitle: "type",
    },
  },
};
