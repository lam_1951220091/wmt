import { HiLink } from "react-icons/hi";

export default {
  name: "resource",
  title: "Resource",
  icon: HiLink,
  type: "document",
  fields: [
    {
      name: "title",
      title: "Title",
      description: "This is the text that will hold the link",
      type: "string",
      validation: (Rule) => Rule.required(),
    },
    {
      name: "type",
      title: "Resource type",
      description:
        "Determines formatting, use 'Online Resource' if it is unclear",
      type: "string",
      validation: (Rule) => Rule.required(),
      options: {
        list: ["Online resource", "Book", "Journal article"],
      },
    },
    {
      name: "author",
      title: "Author",
      description:
        "Can be a single author or organization; use ', editor(s)' for volumes. This will come before the title and link",
      type: "string",
    },
    {
      name: "link",
      title: "Link",
      type: "url",
    },
    {
      name: "date",
      title: "Publication year",
      description:
        "Used for sorting, required (use an access date if you can't find one)",
      type: "number",
      validation: (Rule) => Rule.required(),
      options: {
        initialValue: new Date().getFullYear(),
      },
    },
    {
      name: "publisher",
      title: "Publisher",
      description: "Optional",
      type: "string",
    },
    {
      name: "description",
      title: "Description",
      description: "Write a short pararaph of this post (For SEO Purposes)",
      type: "portableText",
    },
    {
      name: "tags",
      title: "Tags",
      type: "array",
      of: [{ type: "reference", to: { type: "tag" } }],
      validation: (Rule) => Rule.required(),
    },
  ],

  preview: {
    select: {
      title: "title",
      author: "author",
      date: "date",
    },
    prepare(selection) {
      const { title, author, date } = selection;
      const subtitle = `${date}: ${author}`;
      return {
        title,
        subtitle,
      };
    },
  },
};
