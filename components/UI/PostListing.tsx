import Link from "next/link";

type PostListingProps = {
  post: Post;
};

export default function PostListing(props: PostListingProps) {
  const { post } = props;
  const publishedAt = new Date(post.publishedAt);
  return (
    <article>
      <Link href={`/posts/${post.slug.current}`}>
        <a>
          <div className="px-4 pb-4 pt-3 text-slate-500 cursor-pointer rounded-md transition duration-300 hover:bg-slate-100 hover:text-slate-900">
            <p className="inline-block border-b-4 border-slate-500 font-light">
              {publishedAt.toLocaleDateString([], { dateStyle: "long" })}
            </p>
            <h2 className="text-2xl font-bold">{post.title}</h2>
            {post.author && (
              <p className="font-semibold">
                By {post.author.map((author) => author.name).join(", ")}
              </p>
            )}
            {post.description && (
              <p className="text-sm font-light">{post.description}</p>
            )}
          </div>
        </a>
      </Link>
    </article>
  );
}
