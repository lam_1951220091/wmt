import React from "react";
import { ComponentStory, ComponentMeta } from "@storybook/react";

import Tag from "../components/UI/Tag";

export default {
  title: "UI/Tag",
  component: Tag,
  parameters: {
    // More on Story layout: https://storybook.js.org/docs/react/configure/story-layout
    layout: "fullscreen",
  },
} as ComponentMeta<typeof Tag>;

const Template: ComponentStory<typeof Tag> = (args) => <Tag {...args} />;

export const Default = Template.bind({});
Default.args = {
  text: "Special",
};

export const WithColor = Template.bind({});
WithColor.args = {
  text: "Special",
  color: "red",
};
