import React from "react";
import { ComponentStory, ComponentMeta } from "@storybook/react";

import Footer from "../components/Layout/Footer";

export default {
  title: "Layout/Footer",
  component: Footer,
  parameters: {
    layout: "fullscreen",
  },
} as ComponentMeta<typeof Footer>;

const Template: ComponentStory<typeof Footer> = (args) => <Footer {...args} />;

export const Default = Template.bind({});
Default.args = {
  siteConfig: {
    title: "Test title",
    copyrightYear: "2022",
    footerColumns: [
      {
        _key: "1",
        header: "Heading 1",
        entries: [
          { _key: "1", name: "Link 1", link: "https://example.com" },
          { _key: "2", name: "Link 2", link: "https://example.com" },
          {
            _key: "3",
            name: "Link 3 with a really long title to see how wrapping works, Link 3 with a really long title to see how wrapping works, Link 3 with a really long title to see how wrapping works",
            link: "https://example.com",
          },
        ],
      },
      {
        _key: "2",
        header: "Heading 2",
        entries: [
          { _key: "1", name: "Link 1", link: "https://example.com" },
          { _key: "2", name: "Link 2", link: "https://example.com" },
          {
            _key: "3",
            name: "Link 3 with a really long title to see how wrapping works, Link 3 with a really long title to see how wrapping works, Link 3 with a really long title to see how wrapping works",
            link: "https://example.com",
          },
          { _key: "4", name: "Link 4", link: "https://example.com" },
          {
            _key: "5",
            name: "Link 5",
            link: "https://example.com",
          },
        ],
      },
    ],
    social: {
      email: "email@email.com",
      facebook: "https://example.com",
      twitter: "https://example.com",
      gitlab: "https://example.com",
    },
  },
};
