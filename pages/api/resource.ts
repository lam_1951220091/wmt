import { NextApiRequest, NextApiResponse } from "next";

async function sendDiscordMessage(requestBody: {
  name?: string;
  email?: string;
  title?: string;
  description?: string;
}) {
  const { name, email, title, description } = requestBody;
  let result = "";
  if (name && email) {
    result += `${name} (${email})`;
  } else if (name) {
    result += name;
  } else if (email) {
    result += email;
  } else {
    result += "Someone";
  }

  result += " suggested a new resource for the site. ";

  if (title) {
    result += `\nTitle: ${title} `;
  } else {
    result += "\nNo title given ";
  }

  if (description) {
    result += `\nDescription: ${description}`;
  } else {
    result += "\nNo description given";
  }

  await fetch(process.env.DISCORD_WEBHOOK, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      content: result,
    }),
  });
}

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  try {
    await sendDiscordMessage(req.body);
    return res.status(200).json({ success: true });
  } catch (error) {
    console.log(error);
    return res.status(500).json({ error: "There was a problem" });
  }
}
