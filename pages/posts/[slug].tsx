import { useRouter } from "next/router";
import { getClient, usePreviewSubscription, imageBuilder } from "@lib/sanity";
import { groq } from "next-sanity";
import Layout from "@components/Layout";
import NarrowContainer from "@components/Layout/NarrowContainer";
import Prose from "@components/Layout/Prose";
import { H1, H2 } from "@components/Layout/Text";
import { PreviewModeBanner } from "@components/UI/Banner";

const siteConfigQuery = groq`*[_type == "siteconfig"][0]`;

type PostProps = {
  slug: Slug;
  siteConfigData: any;
  postData: Post;
  preview?: boolean;
};

export default function Post(props: PostProps) {
  const { slug, siteConfigData, postData, preview } = props;
  const postQuery = groq`*[_type == "post" && slug.current == "${slug}"]{
    ...,    
    body[]{
      ...,
      markDefs[]{
        ...,
        _type == "internalLink" => {
          _key,
          "slug": @.reference->slug,
          "type": @.reference->_type,
        }
      }
    },
    author[]->,
  }[0]`;
  const router = useRouter();
  const { data: siteConfig } = usePreviewSubscription(siteConfigQuery, {
    initialData: siteConfigData,
    enabled: preview || router.query.preview !== undefined,
  });
  const { data: post } = usePreviewSubscription(postQuery, {
    initialData: postData,
    enabled: preview || router.query.preview !== undefined,
  });

  return (
    post && (
      <Layout
        headerTitle={`${post.title} | ${siteConfig.title}`}
        title={post.title}
        description={post.description}
        image={post.image}
        siteConfig={siteConfig}
      >
        {preview && <PreviewModeBanner />}
        <div className="py-24">
          <NarrowContainer>
            <H1>{post.title}</H1>
            <H2>by {post.author.map((author) => author.name).join(", ")}</H2>
            <Prose body={post.body} />
          </NarrowContainer>
        </div>
      </Layout>
    )
  );
}

export async function getStaticPaths() {
  const slugs = await getClient().fetch(groq`*[_type == 'post']{slug}`);
  return {
    paths: slugs.map((obj: { slug: { current: string } }) => ({
      params: {
        slug: obj.slug.current,
      },
    })),
    fallback: false,
  };
}

export async function getStaticProps({ params, preview = false }) {
  const postQuery = groq`*[_type == "post" && slug.current == "${params.slug}"]{
    ...,
    body[]{
      ...,
      markDefs[]{
        ...,
        _type == "internalLink" => {
          _key,
          "slug": @.reference->slug,
          "type": @.reference->_type,
        }
      }
    },
    author[]->
  }[0]`;
  const siteConfig = await getClient(preview).fetch(siteConfigQuery);
  const post = await getClient(preview).fetch(postQuery);
  return {
    props: {
      slug: params.slug,
      siteConfigData: siteConfig,
      postData: post,
      preview,
    },
    revalidate: 10,
  };
}
