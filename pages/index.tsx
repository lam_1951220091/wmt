import { useRouter } from "next/router";
import { getClient, usePreviewSubscription } from "../lib/sanity";
import { groq } from "next-sanity";
import Layout from "@components/Layout";
import NarrowContainer from "@components/Layout/NarrowContainer";
import Prose from "@components/Layout/Prose";
import PostListing from "@components/UI/PostListing";
import ListContainer from "@components/Layout/ListContainer";
import ChapterListContainer from "@components/Layout/ChapterListContainer";
import Button from "@components/UI/Button";
import { PreviewModeBanner } from "@components/UI/Banner";
import { H1 } from "@components/Layout/Text";

const indexQuery = groq`{
  "siteConfig": *[_type == "siteconfig"][0],
  "page": *[_type == "page" && slug.current == "home"]{
    ...,
    body[]{
      ...,
      markDefs[]{
        ...,
        _type == "internalLink" => {
          _key,
          "slug": @.reference->slug,
          "type": @.reference->_type,
        }
      }
    }
  }[0],
  "chapters": *[_type == "chapter"] | order(publishedAt desc) {
    ..., 
    author[]->,
    tags[]->
  }[0..3],
  "posts": *[_type == "post"] | order(publishedAt desc) {
    ..., 
    author[]->,
    tags[]->
  }[0..3]
}`;

type HomeProps = {
  indexData: { siteConfig: any; page: any; chapters: Chapter[]; posts: Post[] };
  preview?: boolean;
};

export default function Home(props: HomeProps) {
  const { indexData, preview } = props;
  const router = useRouter();
  const { data: index } = usePreviewSubscription(indexQuery, {
    initialData: indexData,
    enabled: preview || router.query.preview !== undefined,
  });
  const { siteConfig, page, chapters, posts } = index;
  return (
    <Layout
      headerTitle={`${[page.title, page.titlePart2].join(" ")} | ${
        siteConfig.title
      }`}
      title={page.title}
      titlePart2={page.titlePart2}
      description={page.description}
      image={page.image}
      cta={page.cta}
      siteConfig={siteConfig}
    >
      {preview && <PreviewModeBanner />}
      <NarrowContainer>
        <div className="pt-20">
          <Prose body={page.body} />
        </div>
      </NarrowContainer>

      <div className="py-8 lg:py-20">
        <H1 center>Recently published chapters</H1>
        <div className="pb-12 pt-6">
          <ChapterListContainer chapters={chapters} />
        </div>
        <NarrowContainer>
          <div className="flex justify-evenly space-x-12">
            <Button link="/chapters" text="See all chapters" />
            <Button link="/resources" text="Other resources" />
          </div>
        </NarrowContainer>
      </div>

      <NarrowContainer>
        <div>
          <H1 center>News and Updates</H1>
          <ListContainer>
            {posts.map((post) => ({
              key: post._id,
              content: <PostListing post={post} />,
            }))}
          </ListContainer>
        </div>
        <div className="flex justify-center space-x-12 pb-24">
          <Button link="/posts" text="See all news" />
          <Button link="/about" text="Learn more" />
        </div>
      </NarrowContainer>
    </Layout>
  );
}

export async function getStaticProps({ params, preview = false }) {
  const indexData = await getClient(preview).fetch(indexQuery);
  return {
    props: {
      indexData,
      preview,
    },
    revalidate: 10,
  };
}
