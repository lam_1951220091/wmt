import { useRouter } from "next/router";
import { getClient, usePreviewSubscription } from "@lib/sanity";
import { groq } from "next-sanity";
import Layout from "@components/Layout";
import NarrowContainer from "@components/Layout/NarrowContainer";
import Prose from "@components/Layout/Prose";
import { PreviewModeBanner } from "@components/UI/Banner";

const siteConfigQuery = groq`*[_type == "siteconfig"][0]`;

type PageProps = {
  slug: { current: string };
  siteConfigData: any;
  pageData: any;
  preview?: boolean;
};

export default function Page(props: PageProps) {
  const { slug, siteConfigData, pageData, preview } = props;
  const pageQuery = groq`*[_type == "page" && slug.current == "${slug}"]{
    ...,
    body[]{
      ...,
      markDefs[]{
        ...,
        _type == "internalLink" => {
          _key,
          "slug": @.reference->slug,
          "type": @.reference->_type,
        }
      }
    }
  }[0]`;
  const router = useRouter();
  const { data: siteConfig } = usePreviewSubscription(siteConfigQuery, {
    initialData: siteConfigData,
    enabled: preview || router.query.preview !== undefined,
  });
  const { data: page } = usePreviewSubscription(pageQuery, {
    initialData: pageData,
    enabled: preview || router.query.preview !== undefined,
  });
  return (
    <Layout
      headerTitle={`${[page.title, page.titlePart2].join(" ")} | ${
        siteConfig.title
      }`}
      title={page.title}
      titlePart2={page.titlePart2}
      description={page.description}
      image={page.image}
      cta={page.cta}
      siteConfig={siteConfig}
    >
      {preview && <PreviewModeBanner />}
      <div className="py-24">
        <NarrowContainer>
          <Prose body={page.body} />
        </NarrowContainer>
      </div>
    </Layout>
  );
}

export async function getStaticPaths() {
  return {
    paths: [{ params: { slug: "about" } }, { params: { slug: "call" } }],
    fallback: false,
  };
}

export async function getStaticProps({ params, preview = false }) {
  const pageQuery = groq`*[_type == "page" && slug.current == "${params.slug}"]{
    ...,
    body[]{
      ...,
      markDefs[]{
        ...,
        _type == "internalLink" => {
          _key,
          "slug": @.reference->slug,
          "type": @.reference->_type,
        }
      }
    }
  }[0]`;
  const siteConfig = await getClient(preview).fetch(siteConfigQuery);
  const page = await getClient(preview).fetch(pageQuery);
  return {
    props: {
      slug: params.slug,
      siteConfigData: siteConfig,
      pageData: page,
      preview,
    },
    revalidate: 10,
  };
}
